import { HelpFormApplication } from "../about/help-form-application.mjs";
import { CONFIG } from "../config.mjs";
import { Logger } from "../logger/logger.mjs";

export class SettingsDialog extends HelpFormApplication {
    #activeStatusEffect = {
        id: null,
        active: true,
        name: null,
        icon: null,
        isCustom: true,
        selected: false,
        allowIconReset: false
    };
    #entryIcon = {
        name: null,
        icon: null,
        active: true,
        isCustom: true,
        selected: false
    };

    constructor(object, options) {
        if (!object) { object = {} };
        object.enableAboutButton = true;

        super(object, options);
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            height: "auto",
            width: 600,
            id: 'coreExpandedSettingsDialog',
            scrollY: [".settings-list"],
            submitOnChange: true,
            tabs: [{ navSelector: ".tabs", contentSelector: "#expandedCore-settings-tabs", initial: "core" }],
            template: CONFIG.TEMPLATES.Settings,
            title: game.i18n.localize("CORE-EXPANDED.SETTINGS.Title")
        }

        return foundry.utils.mergeObject(defaults, overrides);
    }

    async activateListeners(html) {
        super.activateListeners(html);

        html.find('input[name="entryIconIcon"]').on('change', this._onEntryIconChange.bind(this));
        html.find('input[name="statusEffectIcon"]').on('change', this._onStatusIconChange.bind(this));
        html.find('input[name="Core.FilePicker.defaultDirectory"]').on('change', this._onDefaultDirectoryChange.bind(this));
        html.on('click', '[data-action]', this._handleButtonClick.bind(this));
    }

    getData(options) {
        let statusEffects = window.CONFIG.statusEffects.map((s) => { return { id: s.id, icon: s.icon, active: true, label: game.i18n.localize(s.label)}});
        statusEffects = statusEffects.concat(CONFIG.COREEXPANDED.Core.StatusIcons.disabled);
        let displayModes = FilePicker.DISPLAY_MODES.map(d => {
            let label = game.i18n.localize(`FILES.DisplayMode${d.substr(0, 1).toUpperCase() + d.substr(1)}`);
            return {
                key: d,
                label
            }
        });

        let entryIcons = Object.keys(window.CONFIG.JournalEntry.noteIcons).map((k) => { return { name: k, icon: window.CONFIG.JournalEntry.noteIcons[k], active: true}});
        entryIcons = entryIcons.concat(CONFIG.COREEXPANDED.MapNote.Icons.disabled);

        const defaultIcons = entryIcons.filter(i => i.active);

        const partials = {
            core: CONFIG.TEMPLATES.PARTIALS.Core,
            mapnote: CONFIG.TEMPLATES.PARTIALS.MapNote,
            scene: CONFIG.TEMPLATES.PARTIALS.Scene,
            journal: CONFIG.TEMPLATES.PARTIALS.Journal
        }

        const textAnchors = Object.entries(CONST.TEXT_ANCHOR_POINTS).reduce((obj, e) => {
            obj[e[1]] = game.i18n.localize(`JOURNAL.Anchor${e[0].titleCase()}`);
            return obj;
          }, {});

        const fontFamilies = isNewerVersion(CONFIG.FOUNDRYVERSION, "10") ? Object.keys(window.CONFIG.fontDefinitions).reduce((obj, f) => { obj[f] = f; return obj}, {}) : window.CONFIG.fontFamilies.reduce((obj, f) => {
            obj[f] = f;
            return obj;
          }, {});

        let defaultViews = [];
        if (isNewerVersion(CONFIG.FOUNDRYVERSION, "10")) {
            defaultViews = Object.entries(JournalSheet.VIEW_MODES).reduce((obj, e) => { 
                obj[e[1]] = game.i18n.localize(`JOURNAL.View${e[0].titleCase()}`);
                return obj;
            }, {});
        }

        const gridTypes = Object.entries(CONST.GRID_TYPES).reduce((obj, e) => {
            let localizeString = e[0].titleCase();
            switch (e[0]) {
                case "HEXEVENQ":
                    localizeString = "HexEvenQ";
                    break;
                case "HEXEVENR":
                    localizeString = "HexEvenR";
                    break;
                case "HEXODDQ":
                    localizeString = "HexOddQ";
                    break;
                case "HEXODDR":
                    localizeString = "HexOddR";
                    break;
                default:
                    break;
            }

            obj[e[1]] = game.i18n.localize(`SCENES.Grid${localizeString}`);
            return obj;
        }, {});

        const data = {
            activeStatusEffect: this.#activeStatusEffect,
            anchorPoints: textAnchors,
            entryIcon: this.#entryIcon,
            settings: CONFIG.COREEXPANDED,
            displayModes,
            entryIcons,
            fontFamilies,
            options,
            partials,
            defaultIcons,
            defaultViews,
            gridTypes,
            statusEffects,
            isFVTTV9: !(isNewerVersion(CONFIG.FOUNDRYVERSION, "10"))
        };

        Logger.debug(false, "Data", data);

        return data;
    }

    async _updateObject(event, formData) {
        Logger.debug(false, "Update Object", event, formData);
        let formKeys = Object.keys(formData);
        for (let key of formKeys) {
            if (key.startsWith("statusEffect") || key.startsWith("entryIcon")) {
                continue;
            }

            let value = formData[key];
            if (value === "null") { value = null; }
            let parts = key.split('.');

            switch (parts.length) {
                case 5:
                    CONFIG.COREEXPANDED[parts[0]][parts[1]][parts[2]][parts[3]][parts[4]] = value;
                    break;
                case 4:
                    CONFIG.COREEXPANDED[parts[0]][parts[1]][parts[2]][parts[3]] = value;
                    break;
                case 3:
                    CONFIG.COREEXPANDED[parts[0]][parts[1]][parts[2]] = value;
                    break;
                case 2:
                    CONFIG.COREEXPANDED[parts[0]][parts[1]] = value;
                    break;
                default:
                    CONFIG.COREEXPANDED[key] = value;
            }
        }

        if (this.#activeStatusEffect.isCustom) {
            this.#activeStatusEffect.id = formData["statusEffectId"];
            this.#activeStatusEffect.name = formData["statusEffectName"];
        }

        this.#activeStatusEffect.icon = formData["statusEffectIcon"];
        this.#activeStatusEffect.active = formData["statusEffectActive"];

        if (this.#entryIcon.isCustom) {
            this.#entryIcon.name = formData["entryIconName"];
            this.#entryIcon.icon = formData["entryIconIcon"];
        }
        this.#entryIcon.active = formData["entryIconActive"];

        if (CONFIG.COREEXPANDED?.Core?.FilePicker?.displayMode) {
            FilePicker.LAST_DISPLAY_MODE = CONFIG.COREEXPANDED.Core.FilePicker.displayMode;
        }

        if (CONFIG.COREEXPANDED?.Core?.FilePicker?.defaultDirectory) {
            FilePicker.LAST_BROWSED_DIRECTORY = CONFIG.COREEXPANDED.Core.FilePicker.defaultDirectory;
        }

        if (CONFIG.COREEXPANDED.MapNote.TextAnchorPoint) { CONFIG.COREEXPANDED.MapNote.TextAnchorPoint = parseInt(CONFIG.COREEXPANDED.MapNote.TextAnchorPoint); }

        if (CONFIG.COREEXPANDED.Core?.Templates?.Cone?.enableOverride && CONFIG.COREEXPANDED.Core?.Templates?.Cone?.overrideAngle) {
            window.CONFIG.MeasuredTemplate.defaults.angle = CONFIG.COREEXPANDED.Core.Templates.Cone.overrideAngle;
        }
        if (CONFIG.COREEXPANDED.Core?.Templates?.Ray?.enableOverride && CONFIG.COREEXPANDED.Core?.Templates?.Ray?.overrideWidth) {
            window.CONFIG.MeasuredTemplate.defaults.width = CONFIG.COREEXPANDED.Core.Templates.Ray.overrideWidth;
        }

        await game.settings.set(CONFIG.ID, CONFIG.SETTINGS.Settings, CONFIG.COREEXPANDED);

        this.render(true);
    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;
        let id = null;
        let customObject = null;
        let newIcon = null;
        let _this = this;

        Logger.debug(false, "Clicked Event!", event);

        switch (action) {
            case "clearEntryIcon":
                this.#entryIcon = {
                    name: null,
                    icon: null,
                    active: true,
                    isCustom: true,
                    selected: false
                };
                break;
            case "clearStatusEffect":
                this.#activeStatusEffect = {
                    id: null,
                    active: true,
                    name: null,
                    icon: null,
                    isCustom: true,
                    selected: false
                };
                break;
            case "entryIcon":
                id = clickedElement.data().iconId;
                this.#entryIcon.name = id;
                
                customObject = window.CONFIG.JournalEntry.noteIcons[id];
                if (!customObject) {
                    customObject = CONFIG.COREEXPANDED.MapNote.Icons.disabled.find(i => i.name === id);

                    if (!customObject) { return; }
                    this.#entryIcon.isCustom = false;
                    this.#entryIcon.active = false;
                    this.#entryIcon.icon = customObject.icon;
                } else {
                    this.#entryIcon.active = true;
                    this.#entryIcon.isCustom = CONFIG.COREEXPANDED.MapNote.Icons.newIcons.find(i => i.name === id) !== undefined;
                    this.#entryIcon.icon = customObject;
                }

                this.#entryIcon.name = id;
                this.#entryIcon.selected = true;

                break;
            case "resetEntryIcons":
                new Dialog({
                    title: game.i18n.localize("CORE-EXPANDED.MAPNOTES.ENTRYICON.ResetEntryIconsTitle"),
                    content: game.i18n.localize("CORE-EXPANDED.MAPNOTES.ENTRYICON.ResetEntryIconsContent"),
                    buttons: {
                        yes: {
                            label: game.i18n.localize("Yes"),
                            callback: async (html) => {
                                CONFIG.COREEXPANDED.MapNote.Icons.newIcons = [];
                                CONFIG.COREEXPANDED.MapNote.Icons.disabled = [];
                                window.CONFIG.JournalEntry.noteIcons = CONFIG.COREEXPANDED.MapNote.Icons.default;

                                await game.settings.set(CONFIG.ID, CONFIG.SETTINGS.Settings, CONFIG.COREEXPANDED);
                                _this.render(true);
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("Cancel")
                        }
                    },
                    default: "yes"
                }).render(true);
                break;
            case "resetStatusEffects":
                new Dialog({
                    title: game.i18n.localize("CORE-EXPANDED.STATUSEFFECTS.ResetStatusEffectsTitle"),
                    content: game.i18n.localize("CORE-EXPANDED.STATUSEFFECTS.ResetStatusEffectsContent"),
                    buttons: {
                        yes: {
                            label: game.i18n.localize("Yes"),
                            callback: async (html) => {
                                CONFIG.COREEXPANDED.Core.StatusIcons.newIcons = [];
                                CONFIG.COREEXPANDED.Core.StatusIcons.disabled = [];
                                window.CONFIG.statusEffects = deepClone(CONFIG.COREEXPANDED.Core.StatusIcons.default);

                                await game.settings.set(CONFIG.ID, CONFIG.SETTINGS.Settings, CONFIG.COREEXPANDED);
                                _this.render(true);
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("Cancel")
                        }
                    },
                    default: "yes"
                }).render(true);
                break;
            case "status-effect":
                id = clickedElement.data().statusId;
                this.#activeStatusEffect.id = id;
                this.#activeStatusEffect.allowIconReset = false;

                customObject = window.CONFIG.statusEffects.find(s => s.id === id);
                if (!customObject) {
                    customObject = CONFIG.COREEXPANDED.Core.StatusIcons.disabled.find(s => s.id === id);

                    if (!customObject) { return; }
                    this.#activeStatusEffect.isCustom = false;
                    this.#activeStatusEffect.active = false;
                } else {
                    this.#activeStatusEffect.active = true;
                    this.#activeStatusEffect.isCustom = CONFIG.COREEXPANDED.Core.StatusIcons.newIcons.find(s => s.id === id) !== undefined && CONFIG.COREEXPANDED.Core.StatusIcons.default.find(s => s.id === id) === undefined;
                }

                if (!this.#activeStatusEffect.isCustom) {
                    if (customObject.icon !== CONFIG.COREEXPANDED.Core.StatusIcons.default.find(s => s.id === id)?.icon) {
                        //Enable Individual Reset Icon Button
                        this.#activeStatusEffect.allowIconReset = true;
                    }
                }

                this.#activeStatusEffect.name = customObject.label;
                this.#activeStatusEffect.icon = customObject.icon;
                this.#activeStatusEffect.selected = true;
                break;
            case "updateEntryIcon":
                if (this.#entryIcon.isCustom) {
                    customObject = CONFIG.COREEXPANDED.MapNote.Icons.newIcons.find(i => i.name === this.#entryIcon.name);
                    if (!customObject) {
                        newIcon  = {
                            name: this.#entryIcon.name,
                            icon: this.#entryIcon.icon,
                            active: true
                        };

                        CONFIG.COREEXPANDED.MapNote.Icons.newIcons.push(newIcon);
                        window.CONFIG.JournalEntry.noteIcons[newIcon.name] = newIcon.icon;
                    } else {
                        if (this.#entryIcon.active) {
                            customObject.name = this.#entryIcon.name;
                            customObject.icon = this.#entryIcon.icon;
                        } else {
                            CONFIG.COREEXPANDED.MapNote.Icons.newIcons = CONFIG.COREEXPANDED.MapNote.Icons.newIcons.filter(i => i.name !== this.#entryIcon.name);
                            delete(window.CONFIG.JournalEntry.noteIcons[this.#entryIcon.name]);
                        }
                    }
                } else {
                    customObject = window.CONFIG.JournalEntry.noteIcons[this.#entryIcon.name];
                    if (!customObject) {
                        customObject = CONFIG.COREEXPANDED.MapNote.Icons.disabled.find(i => i.name === this.#entryIcon.name);
                    }

                    if (this.#entryIcon.active) {
                        CONFIG.COREEXPANDED.MapNote.Icons.disabled = CONFIG.COREEXPANDED.MapNote.Icons.disabled.filter(i => i.name !== this.#entryIcon.name);
                        if (!window.CONFIG.JournalEntry.noteIcons[this.#entryIcon.name]) {
                            window.CONFIG.JournalEntry.noteIcons[this.#entryIcon.name] = this.#entryIcon.icon;
                        }
                    } else {
                        CONFIG.COREEXPANDED.MapNote.Icons.disabled.push({name: this.#entryIcon.name, icon: customObject});
                        delete(window.CONFIG.JournalEntry.noteIcons[this.#entryIcon.name]);
                    }
                }

                await game.settings.set(CONFIG.ID, CONFIG.SETTINGS.Settings, CONFIG.COREEXPANDED);
                break;
            case "updateStatusEffect":
                if (!this.#activeStatusEffect.id || !this.#activeStatusEffect.name) { 
                    Logger.notify(game.i18n.localize("CORE-EXPANDED.STATUSEFFECTS.WarnIDNameRequired"));
                    return; 
                }
                if (this.#activeStatusEffect.isCustom) {
                    customObject = CONFIG.COREEXPANDED.Core.StatusIcons.newIcons.find(s => s.id === this.#activeStatusEffect.id);
                    if (!customObject) {
                        newIcon = {
                            id: this.#activeStatusEffect.id,
                            label: this.#activeStatusEffect.name,
                            icon: this.#activeStatusEffect.icon,
                            active: true
                        };

                        CONFIG.COREEXPANDED.Core.StatusIcons.newIcons.push(newIcon);
                        window.CONFIG.statusEffects.push(newIcon);
                    } else {
                        if (this.#activeStatusEffect.active) {
                            customObject.label = this.#activeStatusEffect.name;
                            customObject.icon = this.#activeStatusEffect.icon;
                        } else {
                            CONFIG.COREEXPANDED.Core.StatusIcons.newIcons = CONFIG.COREEXPANDED.Core.StatusIcons.newIcons.filter(s => s.id !== this.#activeStatusEffect.id);
                            window.CONFIG.statusEffects = window.CONFIG.statusEffects.filter(s => s.id !== this.#activeStatusEffect.id);
                        }
                    }
                } else {
                    customObject = window.CONFIG.statusEffects.find(s => s.id === this.#activeStatusEffect.id);
                    if (!customObject) {
                        customObject = CONFIG.COREEXPANDED.Core.StatusIcons.disabled.find(s => s.id === this.#activeStatusEffect.id);
                    }

                    if (this.#activeStatusEffect.active) {
                        CONFIG.COREEXPANDED.Core.StatusIcons.disabled = CONFIG.COREEXPANDED.Core.StatusIcons.disabled.filter(s => s.id !== this.#activeStatusEffect.id);

                        if (window.CONFIG.statusEffects.find(s => s.id === this.#activeStatusEffect.id) && window.CONFIG.statusEffects.find(s => s.id === this.#activeStatusEffect.id)?.icon !== this.#activeStatusEffect.icon) {
                            //Icon change!
                            CONFIG.COREEXPANDED.Core.StatusIcons.newIcons.push(this.#activeStatusEffect);
                            this.#activeStatusEffect.allowIconReset = true;
                            window.CONFIG.statusEffects.find(s => s.id === this.#activeStatusEffect.id).icon = this.#activeStatusEffect.icon;
                        } else {
                            window.CONFIG.statusEffects.push(customObject);
                        }
                    } else {
                        CONFIG.COREEXPANDED.Core.StatusIcons.disabled.push(customObject);
                        window.CONFIG.statusEffects = window.CONFIG.statusEffects.filter(s => s.id !== this.#activeStatusEffect.id);
                    }
                }

                await game.settings.set(CONFIG.ID, CONFIG.SETTINGS.Settings, CONFIG.COREEXPANDED);
                break;
            case "reset-icon":
                new Dialog({
                    title: game.i18n.localize("CORE-EXPANDED.STATUSEFFECTS.ResetCoreEffectTitle"),
                    content: game.i18n.localize("CORE-EXPANDED.STATUSEFFECTS.ResetCoreEffectContent"),
                    buttons: {
                        yes: {
                            label: game.i18n.localize("Yes"),
                            callback: async (html) => {
                                this.#activeStatusEffect.icon = window.CONFIG.statusEffects.find(s => s.id === this.#activeStatusEffect.id).icon = CONFIG.COREEXPANDED.Core.StatusIcons.default.find(s => s.id === this.#activeStatusEffect.id).icon;
                                CONFIG.COREEXPANDED.Core.StatusIcons.newIcons = CONFIG.COREEXPANDED.Core.StatusIcons.newIcons.filter(s => s.id !== this.#activeStatusEffect.id);
                                
                                this.#activeStatusEffect.allowIconReset = false;

                                await game.settings.set(CONFIG.ID, CONFIG.SETTINGS.Settings, CONFIG.COREEXPANDED);
                                _this.render(true);
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("Cancel")
                        }
                    },
                    default: "yes"
                }).render(true);
                break;
            default:
                break;
        }

        this.render(true);
    }

    async _onEntryIconChange(event) {
        this.#entryIcon.icon = event.currentTarget.value;
        this.render();
    }

    async _onStatusIconChange(event) {
        this.#activeStatusEffect.icon = event.currentTarget.value;
        this.render();
    }

    async _onDefaultDirectoryChange(event) {
        CONFIG.COREEXPANDED.Core.FilePicker.defaultDirectory = event.currentTarget.value;
    }
}
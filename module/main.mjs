import { CONFIG } from "./config.mjs";
import { Logger } from "./logger/logger.mjs";
import { CoreExpanded } from "./core-expanded.mjs";
import { AboutDialog } from "./about/about-dialog.mjs";

Logger.MODULE_ID = AboutDialog.MODULE_ID = CONFIG.ID;

/*Enable Debug Module */
Hooks.once('devModeReady', ({ registerPackageDebugFlag }) => {
    registerPackageDebugFlag(CONFIG.ID);
});

/* Initialize */
Hooks.once('init', () => { CoreExpanded.initialize(); });

Hooks.once('ready', () => { CoreExpanded.onReady(); });

Hooks.on("setup", () => { CoreExpanded.setupOverrides(); });

/* Add Core Expanded Button */
Hooks.on('renderSidebarTab', async (app, html) => { CoreExpanded.renderSettingsSidebar(app, html) });

/* Scene Configuration */
Hooks.on('preCreateScene', async (scene, params, options, sceneID) => { CoreExpanded.preCreateScene(scene, params, options, sceneID) });

/* Map Note Configuration */
Hooks.on('renderNoteConfig', async (app, html, data) => { CoreExpanded.createMapNote(app, html, data) });

Hooks.on("preUpdateToken", (tokenDoc, update, options) => { CoreExpanded.preUpdateToken(tokenDoc, update, options); });

Hooks.on("renderNoteConfig", (noteConfig, html, data) => { CoreExpanded.renderNoteConfig(noteConfig, html, data); });

/* Journal Configuration */
Hooks.on('renderJournalSheet', (app, html, data) => { CoreExpanded.renderJournalSheet(app, html, data); });
Hooks.on('closeJournalSheet', (app, data) => { CoreExpanded.closeJournalSheet(app, data); });
Hooks.on('renderDocumentSheet', (app, html, data) => { CoreExpanded.renderApplication(app, html, data); })
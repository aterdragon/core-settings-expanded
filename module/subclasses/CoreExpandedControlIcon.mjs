import { CONFIG } from "../config.mjs";

export class CoreExpandedControlIcon extends ControlIcon {
    async draw() {
        this.texture = this.texture ?? await loadTexture(this.iconSrc);

        // Don't draw a destroyed Control
        if ( this.destroyed ) return this;

        // Draw background...except we don't want to do that! muwahahaha!
        //this.bg.clear().beginFill(0x000000, 0.4).lineStyle(2, 0x000000, 1.0).drawRoundedRect(...this.rect, 5).endFill();

        // Draw border
        this.border.clear().lineStyle(2, this.borderColor, 1.0).drawRoundedRect(...this.rect, 5).endFill();
        this.border.visible = false;

        if (game.release?.generation >= 11) { this.bg.clear(); }

        // Draw icon
        this.icon.texture = this.texture;
        this.icon.width = this.icon.height = this.size;
        this.icon.tint = Number.isNumeric(this.tintColor) ? this.tintColor : 0xFFFFFF;
        return this;        
    }
}